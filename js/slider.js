$(function(){
  $sliderContainer = $('#slider');
  $prev = $sliderContainer.find('#prev');
  $next = $sliderContainer.find('#next');

  $prev.on('click', function(){
    var currentImg = $('.active');
    var prevImg = currentImg.next();
    $(".slider__wrapper li:first-child").animate({ marginLeft: -320 }, 100, 'linear', function () {
        $(".slider__wrapper li:first-child").appendTo('.slider__wrapper');
        $(".slider__wrapper li:last-child").css({marginLeft: 0});
        currentImg.removeClass('active');
        prevImg.addClass('active');
    });
  });

  $next.on('click', function(){
    var currentImg = $('.active');
    var nextImg = currentImg.prev();
    $(".slider__wrapper li:first-child").animate({ marginLeft: 320 }, 100, 'linear', function () {
        $(".slider__wrapper li:last-child").prependTo('.slider__wrapper');
        $(".slider__wrapper li:eq('1')").css({marginLeft: 0});
        currentImg.removeClass('active');
        nextImg.addClass('active');
    });
  });

  $carouselContainer = $('#carousel');
  $carouselPrev = $carouselContainer.find('#carousel-prev');
  $carouselNext = $carouselContainer.find('#carousel-next');

  $carouselPrev.on('click', function(){
    var midItem = $('.mid-item');
    var prevItem = midItem.next();
    $('.carousel-wrapper li:first-child').animate({marginLeft: -640}, 100, 'linear', function () {
      $('.carousel-wrapper li:first-child').appendTo('.carousel-wrapper');
      $(".carousel-wrapper li:last-child").css({marginLeft: 0});
      midItem.removeClass('mid-item');
      prevItem.addClass('mid-item');
    });
  });

  $carouselNext.on('click', function(){
    var midItem = $('.mid-item');
    var nextItem = midItem.prev();
    $(".carousel-wrapper li:first-child").animate({ marginLeft: 640 }, 100, 'linear', function () {
        $(".carousel-wrapper li:last-child").prependTo('.carousel-wrapper');
        $(".carousel-wrapper li:eq('1')").css({marginLeft: 0});
        midItem.removeClass('mid-item');
        nextItem.addClass('mid-item');
    });
  });
});
