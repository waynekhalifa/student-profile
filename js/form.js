$(function(){
  $('#first-name').focus(function(){
    $('#first-name-label').css('display', 'block').fadeIn(2000);
  })
  $('#last-name').focus(function(){
    $('#last-name-label').css('display', 'block').fadeIn(2000);
  })
  $('#email').focus(function(){
    $('#email-label').css('display', 'block').fadeIn(2000);
  })
  $('#website').focus(function(){
    $('#website-label').css('display', 'block').fadeIn(2000);
  })
  $('#password').focus(function(){
    $('#password-label').css('display', 'block').fadeIn(2000);
  })
  $('#session-1').on('click', function(){
    $('#lecture-1').slideToggle();
  })
});
